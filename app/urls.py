"""
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from imageUp.views import IndexListView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', IndexListView.as_view(template_name='index.html'), name='index'),
    url(r'^upload_image/$', IndexListView.as_view()),
   
]
