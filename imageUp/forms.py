from django import forms
from .models import Image

class ImageForm(forms.Form):

	title = forms.CharField(max_length=140)
	image = forms.ImageField()

	class Meta:
		model = Image
		fields = ('title', 'image')

form = ImageForm()

