from django.shortcuts import render
from django.views.generic import ListView
from django.views.generic.edit import FormView
from .models import Image
from .forms import ImageForm
from django.http import HttpResponse


# Create your views here.

class IndexListView(FormView, ListView):
	model = Image
	form_class = ImageForm
	template_name = 'index.html'
	context_object_name = 'context'
	

	# def form_valid(self, form):
	# 	if self.request.is_ajax():
	# 		self.object = form.save()
	# 		return HttpResponse(json.dumps("success"),mimetype="application/json")
	# 	return super(IndexListView, self).form_valid(form)

	