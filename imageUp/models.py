from django.db import models
from django.utils import timezone

# Create your models here.

class Image(models.Model):
	title = models.CharField(max_length=140)
	image = models.ImageField(upload_to='static')
	created = models.DateTimeField(default=timezone.now)

	class Meta:
		ordering = ['-created'] 
